import os
import csv

db = {}

def load(path = './playlists/'):
    global db
    path = os.getcwd() + path
    folder = os.listdir(path)
    playlists = [
        os.path.abspath(os.path.join(path, filename)) for filename in folder if filename.endswith('.csv')]

    for filename in playlists:
        id = os.path.basename(filename).replace('.csv', '')
        db[ id ] = []
        with open(filename) as handle:
            contents = csv.DictReader(handle)
            for row in contents:
                db[id].append(dict(row))

def get(id=None):
    return db.get(id, list(db.keys()))

def new(id,items):
    try:
        validID = get(id)
    except LookupError:
        # Good there was an error the id wasn't found. That means it's unique
        db[validID.id] = items

def save():
    # Loop over db and ensure files exist/created
    return 'Save the playlist to file'

def delete(id):
    return 'remove a playlist'

# No required by task.
def update(id,items):
    playlist = get(id)
    new(id, playlist + items)
    return 'Re-created playlist with new items'

if __name__ == '__main__':
    load()
