import csv
import os

db = []

with open(os.getcwd() + '\\data\\song.data.csv') as file:
    reader = csv.DictReader(file)
    for row in reader:
        db.append(row)

def get(key=None):
    for song in db:
        if key == song['title']:
            return song
    return db

def getByGenre(genre):
    return list(filter(lambda s: s['genre'] == genre, db ))
