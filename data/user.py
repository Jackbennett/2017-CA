userList = [
    {'name' : 'Test A'},
    {'name' : 'Test B'}
]

def get(name=None):
    global userList
    if(name):
        for user in userList:
            if user['name'] == name:
                return user
        raise LookupError("User not found")
    return userList

def add(name,DoB='',artist='',genre=''):
    global userList
    try:
        user = get(name)
    except LookupError:
        # New user
        userList.append({
            'name' : name,
            'DoB' : DoB,
            'artist' : artist,
            'genre' : genre
        })
        return True
    raise LookupError('User already exists')

def set(name,key,value):
    global userList
    user = get(name)
    user[key] = value
    return True