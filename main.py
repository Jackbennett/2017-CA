import data.user as u
import data.song as s
import data.playlist as p
import report as r
import math

# tell the playlists file where to get/save data
p.load('.\data\playlists')

def formatduration(time):
    i = int(time)
    m = math.floor(i / 60)
    s = i - m*60
    return "{0}m {1:02}s".format(m, s)

print("{:-^30}".format("Demonstrate User Controls"))

print("Get All Users;")
print(u.get())
print("Specific user A")
print(u.get('Test A'))
print("Try to add user {0}".format(u.add('Test C')))
print("Get All Users;")
print(u.get())
print("Try to update Test A {0}".format(
    u.set('Test A', 'Artist', 'Blink 182')))
print("Specific user A again")
print(u.get('Test A'))

print()
print("{:-^30}".format("List All Songs"))
print("Title    Artist    Genre    Duration")
for song in s.get():
    line = "{0:7}  {1:8}  {2:7}  {3:>8}".format(
            song['title'],
            song['artist'],
            song['genre'],
            formatduration(song['duration']))
    print(line)

print()
print("{:-^30}".format("Report genre duration"))
for g in ['AAA','BBB','CCC']:
    print("Average genre({1}) duration: {0}".format(
        formatduration(r.avgDuration(g)), g))


print()
print("{:-^30}".format("Playlist tess"))
print("Get All Playlist names")
playlists = p.get()
print(playlists)
print("Get Playlist: {0}".format(playlists[0]))
for song in p.get(playlists[0]):
    print("title: {title}, Artist: {artist}".format(**song) +
          ", duration: {0}".format(formatduration(song['duration'])))
