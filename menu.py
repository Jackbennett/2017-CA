import data.song as songs
import data.playlist as pls
import math

pls.load('.\data\playlists')

# Get the local values at this scope.
listfunctions = locals()

def router (page):
    # Execute a function of the page name that has been supplied
    return listfunctions[page]()

def listsongs():
    header('All Songs')
    options(
        formatsong(
            songs.get()),  # Song list
        None,  # Question
        'All Songs',  # title
        "Title    Artist    Genre    Duration"  # column headings row
    )
    router('home')

def selectsongs():
    header('Selection')
    reply = options(
        formatsong(
            songs.get()), # Song list
            'Select song number', # Question
            'All Songs', # title
            "Title    Artist    Genre    Duration" # column headings row
        )
    print("chosen option - {0}".format(reply))
    print(songs.get(getKey(reply)))

def formatsong(song):
    stringlist = []
    for s in song:
        line = "{0:7}  {1:8}  {2:7}  {3:>8}".format(
            s['title'],
            s['artist'],
            s['genre'],
            formatduration(s['duration']))
        stringlist.append({s['title']: line})
    return stringlist

def listplaylist():
    header('All Playlists')
    stringlist = []
    for pl in pls.get():
        line = "{0:80}".format(pl)
        stringlist.append({pl: line})
    return options(stringlist, 'Choose Playlist', 'All Playlists')

def showplaylist():
    chosenlist = getKey(router('listplaylist'))
    header('Songs on Playlist')
    options(formatsong(pls.get(chosenlist)), None,
            'Showing Playlists "{0}"'.format(chosenlist), "Title    Artist    Genre    Duration")
    router('home')


def newplaylist():
    songlist = []
    more = True # At least ask for 1 song by
    name = input("Name: ")
    plID = name.replace(' ','_').lower()
    print("Select a song")
    while more:
        songlist.append(router('selectsongs'))
        print('Total Songs: {0}'.format(len(songlist)))
        reply = input("Choose Another? (y/n): ").lower()
        if reply == 'y':
            more = True
        elif reply == 'n':
            more = False
        else:
            print('Incorrect response')
    pls.new(plID, songlist)

def header(title='Welcome'):
    # Center align the menu tile and add a blank line.
    output = "{:~^80}\n".format('')
    output += "{:^80}\n".format("Song Playlist System")
    output += "{:^80}\n\n".format(title)
    print(str(output))


def options(choicelist, question='Option Choice', title='Options', columnheaders=None):
    # Create a numbered list of options and retrun the chosen option ID
    #  choicelist must be an array of "key:value" pairs for "optionID:menu text"
    if len(choicelist) > 0:
        indexedoptions = [x for x in enumerate(choicelist)]
        output  = "{0}:\n".format(title)
        if columnheaders:
            output += "       {0:73}\n".format(columnheaders)
        for index, value in indexedoptions:
            output += "{0:>4}.  {1:73} \n".format(index + 1, getValue(value))
        print(output)
        if(question):
            choice = int(input("{0}: ".format(question))) - 1 #correct off-by-one humanized list number
            return indexedoptions[choice][1]
    return None

def formatduration(time):
    i = int(time)
    m = math.floor(i / 60)
    s = i - m*60
    return "{0}m {1:02}s".format(m, s)


def getKey(item={}):
    return list(item.keys())[0]
def getValue(item={}):
    return list(item.values())[0]

def home():
    header()
    reply = options([
        {'listsongs': 'Show all songs in library'},
        {'showplaylist': 'Show playlist'},
        {'newplaylist': 'New playlist'}
    ])
    router(getKey(reply))

router('home')
