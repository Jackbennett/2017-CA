# [Task Option 2](https://classroom.google.com/o/NzI0MjUwNDAwMFpa) Requirements

Checkout the "database" branch for the example created for using SQLite

This is an example project using CSV formatted text files as a data store to complete the given task.

### Data
#### Song Table
- title, string, unique
- artist, string, predictive
- genre, string[], common tag list
- duration, int, seconds, format MM/SS at UI.

20 songs, 3 genres and 5 artists.

#### Users Table
- Name, string
- DoB, Date
- Favorite Artist, string, predictive
- Favorite genre, string (possibly descending list.)

#### Files
- Playlist Name, string, unique
- song title.
- song artist.
- song duration.

Load/export CSV or JSON. Style points for using another table.

## Tests
### A
1. [x] Create an account
1. [x] Update Favorite artist and genre

### B
1. [x] Songs, by title including artist, duration

### C
1. [x] Report the average duration of each genre.

### D
1. [] Create, [] save, [x] view and [] export playlists of title, artist and length
1. [] Create playlist under specified total duration
1. [] Create playlist of specified genre, at least 5 songs.
1. [] Create playlist of artists name.

#### Create playlist
1. Show list of songs
1. store choice of songs
1. export choice to file

### E
1. Menu system to choose above options.

Export playlist to CSV or any text file. Note there's no 'update' requirement
