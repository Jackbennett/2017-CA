import data.song as s

def avgDuration(genre):
    query =  s.getByGenre(genre)
    total = 0
    for song in query:
        total += int(song['duration'])
    if(len(query) == 0):
        return 0
    return total / len(query)
